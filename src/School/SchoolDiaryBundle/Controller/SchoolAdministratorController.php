<?php

namespace School\SchoolDiaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use School\SchoolDiaryBundle\Entity\SchoolAdministrator;
use School\SchoolDiaryBundle\Form\SchoolAdministratorType;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use School\UserBundle\Entity\User;

class SchoolAdministratorController extends Controller
{
    public function indexAction($page=null)
    {
        $em = $this->getDoctrine()->getManager();
        $admins = $em->getRepository('SchoolDiaryBundle:SchoolAdministrator')->findBy([]);
        $adapter = new ArrayAdapter($admins);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(1);
        if( !$page ) {
            $page = 1;
        }
        try {
            $pagerfanta->setCurrentPage($page);
        } catch (\NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }
        return $this->render('SchoolDiaryBundle:SchoolAdministrator:index.html.twig', ['pagerfanta' => $pagerfanta]);
    }

    public function newSchoolAdministratorAction(Request $request)
    {
        $admin = new SchoolAdministrator();
        $formType = new SchoolAdministratorType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('new-school-administrator')];
        $form = $this->createForm($formType, $admin, $formOptions);

        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $email = $form->get('email')->getData();
            $checkEmail = $em->getRepository('UserBundle:User')->findOneBy(['email' => $email]);
            if ($checkEmail){
                $error = 'Already taken';
                return $this->render('SchoolDiaryBundle:Teacher:new_teacher.html.twig', ['form' => $form->createView(),
                    'error' => $error
                ]);
            }
            $user = new User();
            $user->setEmail($email);
            $user->setUsername($email);
            $user->setUsernameCanonical($email);
            $user->setPlainPassword('milan');
            $user->setEnabled(true);
            $user->setRoles(['ROLE_SCHOOL_ADMIN']);

            $em->persist($user);
            $em->flush();
            $generatedUser = $em->getRepository('UserBundle:User')->findOneBy(['id' => $user->getId()]);
            $admin->setUser($generatedUser);
            $em->persist($admin);
            $em->flush();

            return $this->redirect($this->generateUrl('school-administrator'));
        }

        return $this->render('SchoolDiaryBundle:SchoolAdministrator:new_school_administrator.html.twig', ['form' => $form->createView()]);
    }

    public function editSchoolAdministratorAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $admin = $em->getRepository('SchoolDiaryBundle:SchoolAdministrator')->find($id);
        $formType = new SchoolAdministratorType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('edit-school-administrator', ['id' => $id])];
        $form = $this->createForm($formType, $admin, $formOptions);
        $form->handleRequest($request);
        if($form->isValid()){
            $em->flush();
            return $this->redirect($this->generateUrl('school-administrator'));
        }
        return $this->render('SchoolDiaryBundle:SchoolAdministrator:edit_school_administrator.html.twig', ['form' => $form->createView()]);
    }

    public function deleteSchoolAdministratorAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $admin = $em->getRepository('SchoolDiaryBundle:SchoolAdministrator')->find($id);
        $em->remove($admin);
        $em->flush();
        return $this->redirect($this->generateUrl('school-administrator'));
    }

    public function schoolAdministratorViewAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $admin = $em->getRepository('SchoolDiaryBundle:SchoolAdministrator')->findOneBy(['user' => $this->getUser()]);

        return $this->render('SchoolDiaryBundle:SchoolAdministrator:view.html.twig', ['admin' => $admin]);
    }

}
