<?php

namespace School\SchoolDiaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use School\SchoolDiaryBundle\Entity\Subject;
use School\SchoolDiaryBundle\Form\SubjectType;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;

class SubjectController extends Controller
{
    public function indexAction($page=null)
    {
        $em = $this->getDoctrine()->getManager();
        $subjects = $em->getRepository('SchoolDiaryBundle:Subject')->findBy([]);
        $adapter = new ArrayAdapter($subjects);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(1);
        if( !$page ) {
            $page = 1;
        }
        try {
            $pagerfanta->setCurrentPage($page);
        } catch (\NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }
        return $this->render('SchoolDiaryBundle:Subject:index.html.twig', ['pagerfanta' => $pagerfanta]);
    }

    public function newSubjectAction(Request $request)
    {
        $subject = new Subject();
        $formType = new SubjectType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('new-subject')];
        $form = $this->createForm($formType, $subject, $formOptions);

        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($subject);
            $em->flush();

            return $this->redirect($this->generateUrl('subject'));
        }

        return $this->render('SchoolDiaryBundle:Subject:new_subject.html.twig', ['form' => $form->createView()]);
    }

    public function editSubjectAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $subject = $em->getRepository('SchoolDiaryBundle:Subject')->findOneBy(['id' => $id]);
        $formType = new SubjectType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('edit-subject', ['id' => $id])];
        $form = $this->createForm($formType, $subject, $formOptions);
        $form->handleRequest($request);
        if($form->isValid()){
            $em->flush();
            return $this->redirect($this->generateUrl('subject'));
        }
        return $this->render('SchoolDiaryBundle:Subject:edit_subject.html.twig', ['form' => $form->createView()]);
    }

    public function deleteSubjectAction($id)
    {
        $em= $this->getDoctrine()->getManager();
        $subject = $em->getRepository('SchoolDiaryBundle:Subject')->find($id);
        $em->remove($subject);
        $em->flush();
        return $this->redirect($this->generateUrl('subject'));
    }

}
