<?php

namespace School\SchoolDiaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use School\SchoolDiaryBundle\Entity\Region;
use School\SchoolDiaryBundle\Form\RegionType;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;

class RegionController extends Controller
{
    public function indexAction($page=null)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SchoolDiaryBundle:Region')->findBy([]);
        $adapter = new ArrayAdapter($regions);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(1);
        if( !$page ) {
            $page = 1;
        }
        try {
            $pagerfanta->setCurrentPage($page);
        } catch (\NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }
        return $this->render('SchoolDiaryBundle:Region:index.html.twig', ['pagerfanta' => $pagerfanta]);
    }

    public function newRegionAction(Request $request)
    {
        $region = new Region();
        $formType = new RegionType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('new-region')];
        $form = $this->createForm($formType, $region, $formOptions);

        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($region);
            $em->flush();

            return $this->redirect($this->generateUrl('region'));
        }

        return $this->render('SchoolDiaryBundle:Region:new_region.html.twig', ['form' => $form->createView()]);
    }

    public function editRegionAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $region = $em->getRepository('SchoolDiaryBundle:Region')->findOneBy(['id' => $id]);
        $formType = new RegionType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('edit-region', ['id' => $id])];
        $form = $this->createForm($formType, $region, $formOptions);
        $form->handleRequest($request);
        if($form->isValid()){
            $em->flush();
            return $this->redirect($this->generateUrl('region'));
        }
        return $this->render('SchoolDiaryBundle:Region:edit_region.html.twig', ['form' => $form->createView()]);
    }

    public function deleteRegionAction($id)
    {
        $em= $this->getDoctrine()->getManager();
        $region = $em->getRepository('SchoolDiaryBundle:Region')->find($id);
        $em->remove($region);
        $em->flush();
        return $this->redirect($this->generateUrl('region'));
    }

}
