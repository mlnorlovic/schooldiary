<?php

namespace School\SchoolDiaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use School\SchoolDiaryBundle\Entity\Municipality;
use School\SchoolDiaryBundle\Form\MunicipalityType;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $securityContext = $this->container->get('security.context');
        if($securityContext->isGranted('ROLE_TEACHER')) {
            return $this->redirect($this->generateUrl('teacher-view'));
        }elseif($securityContext->isGranted('ROLE_SCHOOL_ADMIN')){
            return $this->redirect($this->generateUrl('school-administrator-view'));
        }elseif($securityContext->isGranted('ROLE_ADMIN')){
            return $this->redirect($this->generateUrl('admin-view'));
        }else{
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
    }
    public function adminViewAction()
    {
        return $this->render('SchoolDiaryBundle:Default:view.html.twig');
    }
}
