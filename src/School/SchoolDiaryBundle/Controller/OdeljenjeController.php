<?php

namespace School\SchoolDiaryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use School\SchoolDiaryBundle\Entity\Odeljenje;
use School\SchoolDiaryBundle\Entity\ClassSubjectTeacher;
use School\SchoolDiaryBundle\Form\OdeljenjeType;
use School\SchoolDiaryBundle\Form\SubjectTeacherType;

use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;

use Doctrine\ORM\EntityRepository;

/**
 * Odeljenje controller.
 *
 */
class OdeljenjeController extends Controller
{

    /**
     * Lists all Odeljenje entities.
     *
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SchoolDiaryBundle:Odeljenje')->findBy(['school' => $this->getCurrentSchool()->getId()]);

        $adapter = new ArrayAdapter($entities);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(2);
        if( !$page ) {
            $page = 1;
        }
        try {
            $pagerfanta->setCurrentPage($page);
        } catch (\NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }

        return $this->render('SchoolDiaryBundle:Odeljenje:index.html.twig', array(
            'entities' => $entities,
            'pagerfanta' => $pagerfanta
        ));
    }
    /**
     * Creates a new Odeljenje entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Odeljenje();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setSchool($this->getCurrentSchool());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('class-show', array('id' => $entity->getId())));
        }

        return $this->render('SchoolDiaryBundle:Odeljenje:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Odeljenje entity.
     *
     * @param Odeljenje $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Odeljenje $entity)
    {
        $form = $this->createForm(new OdeljenjeType(), $entity, array(
            'action' => $this->generateUrl('class-create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Odeljenje entity.
     *
     */
    public function newAction()
    {
        $entity = new Odeljenje();
        $form   = $this->createCreateForm($entity);

        return $this->render('SchoolDiaryBundle:Odeljenje:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Odeljenje entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SchoolDiaryBundle:Odeljenje')->find($id);
        $cst = $em->getRepository('SchoolDiaryBundle:ClassSubjectTeacher')->findByClassId($id);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Odeljenje entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SchoolDiaryBundle:Odeljenje:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'cst' => $cst,
        ));
    }

    /**
     * Displays a form to edit an existing Odeljenje entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SchoolDiaryBundle:Odeljenje')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Odeljenje entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SchoolDiaryBundle:Odeljenje:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Odeljenje entity.
    *
    * @param Odeljenje $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Odeljenje $entity)
    {
        $form = $this->createForm(new OdeljenjeType(), $entity, array(
            'action' => $this->generateUrl('class-update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Odeljenje entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SchoolDiaryBundle:Odeljenje')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Odeljenje entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('class-edit', array('id' => $id)));
        }

        return $this->render('SchoolDiaryBundle:Odeljenje:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Odeljenje entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SchoolDiaryBundle:Odeljenje')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Odeljenje entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('class'));
    }

    /**
     * Creates a form to delete a Odeljenje entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('class-delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function addSubjectsAction(Request $request, $classId)
    {
        $em = $this->getDoctrine()->getManager();
        $formType = new SubjectTeacherType();
        $formOptions = ['action' => $this->generateUrl('subject-teacher', ['classId' => $classId])];
        $form = $this->createForm($formType, null, $formOptions);
        $form->add('teacher', 'entity', array(
            'class' => 'SchoolDiaryBundle:Teacher',
            'property' => 'fullName',
            'multiple' => false,
            'empty_data' => false,
            'empty_value' => 'Izaberite nastavnika',
            'invalid_message' => 'Izaberite nastavnika',
            'mapped' => false,
            'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('t')
                    ->innerJoin('t.school', 's')
                    ->where('s.id = :school')
                    ->setParameter('school', $this->getCurrentSchool());
            }
        ));
        $odeljenje = $em->getRepository('SchoolDiaryBundle:Odeljenje')->find($classId);
        $list = $em->getRepository('SchoolDiaryBundle:ClassSubjectTeacher')->findByClassId($classId);
        $form->handleRequest($request);

        if ($request->isMethod('POST')){
            $subjectId = $form->get('subject')->getData()->getId();
            $teacherId = $form->get('teacher')->getData()->getId();
            $teacher = $em->getRepository('SchoolDiaryBundle:Teacher')->find($teacherId);
            $subject = $em->getRepository('SchoolDiaryBundle:Subject')->find($subjectId);
            $taken = $em->getRepository('SchoolDiaryBundle:ClassSubjectTeacher')->findOneBy(['subjectId' => $subject, 'classId' => $odeljenje]);

            if($taken){
                $error = 'Ovaj predmet je vec unet';
                return $this->render('SchoolDiaryBundle:Odeljenje:class_subject_teacher.html.twig',
                    [
                        'form' => $form->createView(),
                        'list' => $list,
                        'odeljenje' => $odeljenje,
                        'error' => $error
                    ]);
            }

            $cst = new ClassSubjectTeacher();
            $cst->setClassId($odeljenje);
            $cst->setSubjectId($subject);
            $cst->setTeacherId($teacher);
            $em->persist($cst);
            $em->flush();
            return $this->redirect($this->generateUrl('subject-teacher', ['classId' => $classId]));
        }

        return $this->render('SchoolDiaryBundle:Odeljenje:class_subject_teacher.html.twig',
            [
                'form' => $form->createView(),
                'list' => $list,
                'odeljenje' => $odeljenje
            ]);
    }

    public function editSubjectsAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SchoolDiaryBundle:ClassSubjectTeacher')->find($id);
        $formType = new SubjectTeacherType();
        $formOptions = ['action' => $this->generateUrl('edit-subject-teacher', ['id' => $id])];
        $form = $this->createForm($formType, null, $formOptions);
        $form->remove('subject');
        $form->add('teacher', 'entity', array(
            'class' => 'SchoolDiaryBundle:Teacher',
            'property' => 'fullName',
            'multiple' => false,
            'empty_data' => false,
            'invalid_message' => 'Izaberite nastavnika',
            'mapped' => false,
            'empty_value' => 'Izaberite nastavnika',
            'data' => $entity->getTeacherId(),
            'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('t')
                    ->innerJoin('t.school', 's')
                    ->where('s.id = :school')
                    ->setParameter('school', $this->getCurrentSchool());
            }
        ));
        $form->handleRequest($request);
        if($request->isMethod('POST')){
            $select = $form->get('teacher')->getData();
            $teacher = $em->getRepository('SchoolDiaryBundle:Teacher')->find($select);
            $entity->setTeacherId($teacher);
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('subject-teacher', ['classId' => $entity->getClassId()]));
        }
        return $this->render('SchoolDiaryBundle:Odeljenje:edit_subject_teacher.html.twig',
            [
                'entity' => $entity,
                'form' => $form->createView()
            ]);
    }

    public function deleteSubjectsAction($id)
    {
        $em= $this->getDoctrine()->getManager();
        $subject = $em->getRepository('SchoolDiaryBundle:ClassSubjectTeacher')->find($id);
        $em->remove($subject);
        $em->flush();
        return $this->redirect($this->generateUrl('subject-teacher', ['classId' => $subject->getClassId()]));
    }

    public function getCurrentSchool()
    {
        $em = $this->getDoctrine()->getManager();
        $admin = $em->getRepository('SchoolDiaryBundle:SchoolAdministrator')->findOneBy(['user' => $this->getUser()]);
        $school = $admin->getSchool();

        return $school;
    }
}
