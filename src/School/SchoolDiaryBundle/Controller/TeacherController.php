<?php

namespace School\SchoolDiaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use School\SchoolDiaryBundle\Entity\Teacher;
use School\SchoolDiaryBundle\Entity\TeacherSubjects;
use School\SchoolDiaryBundle\Form\TeacherType;
use School\SchoolDiaryBundle\Form\SearchType;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use School\UserBundle\Entity\User;

class TeacherController extends Controller
{
    public function indexAction($page=null)
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');
        if($securityContext->isGranted('ROLE_ADMIN')) {
            $teachers = $em->getRepository('SchoolDiaryBundle:Teacher')->findBy([]);
        }else{
            $currentUser = $this->getUser();
            $admin = $em->getRepository('SchoolDiaryBundle:SchoolAdministrator')->findOneBy(['user'=>$currentUser]);
            $school = $admin->getSchool()->getId();
            $teachers = $em->getRepository('SchoolDiaryBundle:Teacher')->getTeachersBySchool($school);
        }
        $adapter = new ArrayAdapter($teachers);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(1);
        if( !$page ) {
            $page = 1;
        }
        try {
            $pagerfanta->setCurrentPage($page);
        } catch (\NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }
        return $this->render('SchoolDiaryBundle:Teacher:index.html.twig', ['pagerfanta' => $pagerfanta]);
    }

    public function newTeacherAction(Request $request)
    {
        $session = $request->getSession();
        $teacher = new Teacher();
        $formType = new TeacherType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('new-teacher')];
        $teacher->setIdentificationNumber($session->get('identificationNumber'));
        $form = $this->createForm($formType, $teacher, $formOptions);
        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $email = $form->get('email')->getData();
            $checkEmail = $em->getRepository('UserBundle:User')->findOneBy(['email' => $email]);
            if ($checkEmail){
                $error = 'Already taken';
                return $this->render('SchoolDiaryBundle:Teacher:new_teacher.html.twig', ['form' => $form->createView(),
                'error' => $error
                ]);
            }
            $user = new User();
            $user->setEmail($email);
            $user->setUsername($email);
            $user->setUsernameCanonical(strtolower($email));
            $user->setPlainPassword('milan');
            $user->setEnabled(true);
            $user->setRoles(['ROLE_TEACHER']);
            $em->persist($user);
            $em->flush();

            $generatedUser = $em->getRepository('UserBundle:User')->findOneBy(['id' => $user->getId()]);
            $currentUser = $this->getUser();
            $admin = $em->getRepository('SchoolDiaryBundle:SchoolAdministrator')->findOneBy(['user'=>$currentUser]);
            $adminSchool = $admin->getSchool();
            $school = $em->getRepository('SchoolDiaryBundle:School')->findOneBy(['id'=>$adminSchool]);
            $teacher->getSchool()->add($school);
            $teacher->setUser($generatedUser);
            $em->persist($teacher);
            $em->flush();

            foreach($form->get('subject')->getData() as $subject){
                $ts = new TeacherSubjects();
                $ts->setSubjects($subject->getId());
                $ts->setTeacher($teacher->getId());
                $ts->setSchool($school->getId());
                $em->persist($ts);
                $em->flush();
            }

            return $this->redirect($this->generateUrl('teacher'));
        }

        return $this->render('SchoolDiaryBundle:Teacher:new_teacher.html.twig', ['form' => $form->createView()]);
    }

    public function editTeacherAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher = $em->getRepository('SchoolDiaryBundle:Teacher')->findOneBy(['id' => $id]);
        $formType = new TeacherType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('edit-teacher', ['id' => $id])];
        $form = $this->createForm($formType, $teacher, $formOptions);
        $form->remove('email');
        $form->handleRequest($request);
        if($form->isValid()){
            $em->flush();
            return $this->redirect($this->generateUrl('teacher'));
        }
        return $this->render('SchoolDiaryBundle:Teacher:edit_teacher.html.twig', ['form' => $form->createView()]);
    }

    public function deleteTeacherAction($id)
    {
        $em= $this->getDoctrine()->getManager();
        $teacher = $em->getRepository('SchoolDiaryBundle:Teacher')->find($id);
        $em->remove($teacher);
        $em->flush();
        return $this->redirect($this->generateUrl('teacher'));
    }

    public function removeTeacherAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $currentUser = $this->getUser();
        $admin = $em->getRepository('SchoolDiaryBundle:SchoolAdministrator')->findOneBy(['user'=>$currentUser]);
        $school = $admin->getSchool();
        $teacher = $em->getRepository('SchoolDiaryBundle:Teacher')->findOneBy(['id' => $id]);
        $teacher->getSchool()->removeElement($school);
        $em->persist($teacher);
        $em->flush();
        return $this->redirect($this->generateUrl('teacher'));
    }

    public function teacherViewAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher = $em->getRepository('SchoolDiaryBundle:Teacher')->findOneBy(['user' => $this->getUser()]);

        return $this->render('SchoolDiaryBundle:Teacher:view.html.twig', ['teacher' => $teacher]);
    }

    public function teacherSearchAction(Request $request)
    {
        $formType = new SearchType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('teacher-search')];
        $form = $this->createForm($formType, null, $formOptions);
        $form->handleRequest($request);
        $searchText = $form->get('searchText')->getData();
        if ($form->isValid()){
            $session = $request->getSession();
            $session->set('identificationNumber', $searchText);
            $em = $this->getDoctrine()->getManager();
            $teacher = $em->getRepository('SchoolDiaryBundle:Teacher')->findOneBy(['identificationNumber' => $searchText]);

            if ($teacher){
                return $this->redirect($this->generateUrl('add-existing-teacher'));
            }else{
                return $this->redirect($this->generateUrl('new-teacher'));
            }
        }

        return $this->render('SchoolDiaryBundle:Teacher:search.html.twig', ['form' => $form->createView()]);
    }

    public function addExistingTeacherAction(Request $request){
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $identificationNumber = $session->get('identificationNumber');
        $teacher = $em->getRepository('SchoolDiaryBundle:Teacher')->findOneBy(['identificationNumber' => $identificationNumber]);

        return $this->render('SchoolDiaryBundle:Teacher:add_existing_teacher.html.twig', ['teacher' => $teacher]);
    }

    public function saveTeacherAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $currentUser = $this->getUser();
        $admin = $em->getRepository('SchoolDiaryBundle:SchoolAdministrator')->findOneBy(['user'=>$currentUser]);
        $school = $admin->getSchool();
        $teacher = $em->getRepository('SchoolDiaryBundle:Teacher')->findOneBy(['id' => $id]);

        if (!$teacher->getSchool()->contains($school)){
            $teacher->getSchool()->add($school);
            $em->persist($teacher);
            $em->flush();
        }else{
            $error = 'Ovaj nastavnik je vec prijavljen u ovoj skoli';
            return $this->render('SchoolDiaryBundle:Teacher:add_existing_teacher.html.twig', [
                'teacher' => $teacher, 'error' => $error
            ]);
        }

        return $this->redirect($this->generateUrl('teacher'));
    }

}
