<?php

namespace School\SchoolDiaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use School\SchoolDiaryBundle\Entity\Municipality;
use School\SchoolDiaryBundle\Form\MunicipalityType;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;

class MunicipalityController extends Controller
{
    public function indexAction($page=null)
    {
        $em = $this->getDoctrine()->getManager();
        $municipalities = $em->getRepository('SchoolDiaryBundle:Municipality')->findBy([]);
        $adapter = new ArrayAdapter($municipalities);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(1);
        if( !$page ) {
            $page = 1;
        }
        try {
            $pagerfanta->setCurrentPage($page);
        } catch (\NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }
        return $this->render('SchoolDiaryBundle:Municipality:index.html.twig', ['pagerfanta' => $pagerfanta]);
    }

    public function newMunicipalityAction(Request $request)
    {
        $region = new Municipality();
        $formType = new MunicipalityType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('new-municipality')];
        $form = $this->createForm($formType, $region, $formOptions);

        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($region);
            $em->flush();

            return $this->redirect($this->generateUrl('municipality'));
        }

        return $this->render('SchoolDiaryBundle:Municipality:new_municipality.html.twig', ['form' => $form->createView()]);
    }

    public function editMunicipalityAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $region = $em->getRepository('SchoolDiaryBundle:Municipality')->findOneBy(['id' => $id]);
        $formType = new MunicipalityType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('edit-municipality', ['id' => $id])];
        $form = $this->createForm($formType, $region, $formOptions);
        $form->handleRequest($request);
        if($form->isValid()){
            $em->flush();
            return $this->redirect($this->generateUrl('municipality'));
        }
        return $this->render('SchoolDiaryBundle:Municipality:edit_municipality.html.twig', ['form' => $form->createView()]);
    }

    public function deleteMunicipalityAction($id)
    {
        $em= $this->getDoctrine()->getManager();
        $municipality = $em->getRepository('SchoolDiaryBundle:Municipality')->find($id);
        $em->remove($municipality);
        $em->flush();
        return $this->redirect($this->generateUrl('municipality'));
    }

}
