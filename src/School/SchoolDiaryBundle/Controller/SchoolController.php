<?php

namespace School\SchoolDiaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use School\SchoolDiaryBundle\Entity\School;
use School\SchoolDiaryBundle\Form\SchoolType;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;

class SchoolController extends Controller
{
    public function indexAction($page=null)
    {
        $em = $this->getDoctrine()->getManager();
        $schools = $em->getRepository('SchoolDiaryBundle:School')->findBy([]);
        $adapter = new ArrayAdapter($schools);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(1);
        if( !$page ) {
            $page = 1;
        }
        try {
            $pagerfanta->setCurrentPage($page);
        } catch (\NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }
        return $this->render('SchoolDiaryBundle:School:index.html.twig', ['pagerfanta' => $pagerfanta]);
    }

    public function newSchoolAction(Request $request)
    {
        $school = new School();
        $formType = new SchoolType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('new-school')];
        $form = $this->createForm($formType, $school, $formOptions);

        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($school);
            $em->flush();

            return $this->redirect($this->generateUrl('school'));
        }

        return $this->render('SchoolDiaryBundle:School:new_school.html.twig', ['form' => $form->createView()]);
    }

    public function editSchoolAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $school = $em->getRepository('SchoolDiaryBundle:School')->findOneBy(['id' => $id]);
        $formType = new SchoolType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('edit-school', ['id' => $id])];
        $form = $this->createForm($formType, $school, $formOptions);
        $form->handleRequest($request);
        if($form->isValid()){
            $em->flush();
            return $this->redirect($this->generateUrl('school'));
        }
        return $this->render('SchoolDiaryBundle:School:edit_school.html.twig', ['form' => $form->createView()]);
    }

    public function deleteSchoolAction($id)
    {
        $em= $this->getDoctrine()->getManager();
        $school = $em->getRepository('SchoolDiaryBundle:School')->find($id);
        $em->remove($school);
        $em->flush();
        return $this->redirect($this->generateUrl('school'));
    }

}
