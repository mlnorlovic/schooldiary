<?php

namespace School\SchoolDiaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use School\SchoolDiaryBundle\Entity\Student;
use School\SchoolDiaryBundle\Form\StudentType;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use Doctrine\ORM\EntityRepository;

class StudentController extends Controller
{
    public function indexAction($page=null)
    {
        $em = $this->getDoctrine()->getManager();
        $students = $em->getRepository('SchoolDiaryBundle:Student')->findBy([]);
        $adapter = new ArrayAdapter($students);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(1);
        if( !$page ) {
            $page = 1;
        }
        try {
            $pagerfanta->setCurrentPage($page);
        } catch (\NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }
        return $this->render('SchoolDiaryBundle:Student:index.html.twig', ['pagerfanta' => $pagerfanta]);
    }

    public function newStudentAction(Request $request)
    {
        $student = new Student();
        $formType = new StudentType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('new-student')];
        $form = $this->createForm($formType, $student, $formOptions);
        $form->add('razred', 'entity', array(
            'class' => 'SchoolDiaryBundle:Odeljenje',
            'property' => 'razred',
            'multiple' => false,
            'empty_data' => false,
            'empty_value' => 'Izaberite razred',
            'invalid_message' => 'Izaberite razred',
            'mapped' => false,
            'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('o')
                    ->innerJoin('o.school', 's')
                    ->where('s.id = :school')
                    ->setParameter('school', $this->getCurrentSchool());
            }
        ))
            ->add('odeljenje', 'entity', array(
                'class' => 'SchoolDiaryBundle:Odeljenje',
                'property' => 'odeljenje',
                'multiple' => false,
                'empty_data' => false,
                'empty_value' => 'Izaberite odeljenje',
                'invalid_message' => 'Izaberite odeljenje',
                'mapped' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->innerJoin('o.school', 's')
                        ->where('s.id = :school')
                        ->setParameter('school', $this->getCurrentSchool());
                }
            ));

        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($student);
            $em->flush();

            return $this->redirect($this->generateUrl('student'));
        }

        return $this->render('SchoolDiaryBundle:Student:new_student.html.twig', ['form' => $form->createView()]);
    }

    public function editStudentAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('SchoolDiaryBundle:Student')->findOneBy(['id' => $id]);
        $formType = new StudentType();
        $formOptions = ['method' => 'POST', 'action' => $this->generateUrl('edit-student', ['id' => $id])];
        $form = $this->createForm($formType, $student, $formOptions);
        $form->handleRequest($request);
        if($form->isValid()){
            $em->flush();
            return $this->redirect($this->generateUrl('student'));
        }
        return $this->render('SchoolDiaryBundle:Student:edit_student.html.twig', ['form' => $form->createView()]);
    }

    public function deleteStudentAction($id)
    {
        $em= $this->getDoctrine()->getManager();
        $student = $em->getRepository('SchoolDiaryBundle:Student')->find($id);
        $em->remove($student);
        $em->flush();
        return $this->redirect($this->generateUrl('student'));
    }

    public function getCurrentSchool()
    {
        $em = $this->getDoctrine()->getManager();
        $admin = $em->getRepository('SchoolDiaryBundle:SchoolAdministrator')->findOneBy(['user' => $this->getUser()]);
        $school = $admin->getSchool();

        return $school;
    }

}
