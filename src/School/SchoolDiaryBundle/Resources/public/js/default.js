$(function(){
    $('.del').on('click', function(e){
        e.preventDefault();
        var del = $('.del'),
            url = del.attr('href'),
            id = del.attr('data-id'),
            result = confirm("Da li ste sigurni da zelte da isbrisete podatak sa rednim brojem" + " " + id);
        result == true ? window.location = url : window.close();
    });
});

$(document).ready(function() {
    $('#school_schooldiarybundle_teacher_subject').multiselect();
});
