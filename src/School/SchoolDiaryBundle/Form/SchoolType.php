<?php

namespace School\SchoolDiaryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SchoolType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('type', 'choice', array(
                'choices'   => array('osnovna' => 'Osnovna skola', 'srednja' => 'Srednja skola'),
                'required'  => false,
                'empty_value' => 'Izaberite tip skole'
            ))
            ->add('address')
            ->add('web')
            ->add('email')
            ->add('phone')
            ->add('municipality', 'entity', array(
                'class' => 'SchoolDiaryBundle:Municipality',
                'property' => 'name',
                'empty_value' => 'Izaberite opstinu'
            ))
            ->add('submit', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'School\SchoolDiaryBundle\Entity\School'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'school_schooldiarybundle_school';
    }
}
