<?php

namespace School\SchoolDiaryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use School\UserBundle\Form\RegistrationFormType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class SchoolAdministratorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('parentName')
            ->add('identificationNumber')
            ->add('school', 'entity', array(
                'class' => 'SchoolDiaryBundle:School',
                'property' => 'name',
                'empty_value' => 'Izaberite skolu'
            ))
            ->add('email', 'text', array('mapped' => false, 'constraints' => array(
                new Email(), new NotBlank()
            )
            ))
            ->add('submit', 'submit')
//            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
//                function generateRandomString($length = 7) {
//                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//                    $charactersLength = strlen($characters);
//                    $randomString = '';
//                    for ($i = 0; $i < $length; $i++) {
//                        $randomString .= $characters[rand(0, $charactersLength - 1)];
//                    }
//                    return $randomString;
//                }
//                $data = $event->getData();
//                $data['user']['plainPassword'] = 'milan';
//                $event->setData($data);
//            })
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'School\SchoolDiaryBundle\Entity\SchoolAdministrator',
            'validation_groups' => array('Registration'),
            'cascade_validation' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'school_schooldiarybundle_schooladministrator';
    }
}
