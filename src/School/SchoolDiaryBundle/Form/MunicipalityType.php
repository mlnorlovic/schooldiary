<?php

namespace School\SchoolDiaryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class MunicipalityType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('region', 'entity', array(
                  'class' => 'SchoolDiaryBundle:Region',
                  'property' => 'name',
                  'empty_value' => 'Izaberite region'
            ))
            ->add('submit', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'School\SchoolDiaryBundle\Entity\Municipality'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'school_schooldiarybundle_municipality';
    }
}
