<?php

namespace School\SchoolDiaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Odeljenje
 *
 * @ORM\Table(name="odeljenje")
 * @ORM\Entity(repositoryClass="School\SchoolDiaryBundle\Entity\OdeljenjeRepository")
 */
class Odeljenje
{
    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="razred", type="integer")
     */
    private $razred;

    /**
     * @var integer
     *
     * @ORM\Column(name="odeljenje", type="integer")
     */
    private $odeljenje;

    /**
     * @var string
     *
     * @ORM\Column(name="razredni", type="string", length=255)
     */
    private $razredni;

    /**
     * @var integer
     *
     * @ORM\Column(name="generacija", type="integer")
     */
    private $generacija;

    /**
     * @ORM\ManyToOne(targetEntity="School", cascade={"persist"})
     * @ORM\JoinColumn(name="school_id", referencedColumnName="id")
     */
    protected $school;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set razred
     *
     * @param integer $razred
     * @return Odeljenje
     */
    public function setRazred($razred)
    {
        $this->razred = $razred;

        return $this;
    }

    /**
     * Get razred
     *
     * @return integer 
     */
    public function getRazred()
    {
        return $this->razred;
    }

    /**
     * Set odeljenje
     *
     * @param integer $odeljenje
     * @return Odeljenje
     */
    public function setOdeljenje($odeljenje)
    {
        $this->odeljenje = $odeljenje;

        return $this;
    }

    /**
     * Get odeljenje
     *
     * @return integer 
     */
    public function getOdeljenje()
    {
        return $this->odeljenje;
    }

    /**
     * Set razredni
     *
     * @param string $razredni
     * @return Odeljenje
     */
    public function setRazredni($razredni)
    {
        $this->razredni = $razredni;

        return $this;
    }

    /**
     * Get razredni
     *
     * @return string 
     */
    public function getRazredni()
    {
        return $this->razredni;
    }

    /**
     * Set generacija
     *
     * @param integer $generacija
     * @return Odeljenje
     */
    public function setGeneracija($generacija)
    {
        $this->generacija = $generacija;

        return $this;
    }

    /**
     * Get generacija
     *
     * @return integer 
     */
    public function getGeneracija()
    {
        return $this->generacija;
    }

    /**
     * @return mixed
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * @param mixed $school
     */
    public function setSchool($school)
    {
        $this->school = $school;
    }
}
