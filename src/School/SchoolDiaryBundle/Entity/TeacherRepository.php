<?php

namespace School\SchoolDiaryBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TeacherRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TeacherRepository extends EntityRepository
{
    public function getTeachersBySchool($school)
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.school', 's')
            ->where('s.id = :school')
            ->setParameter('school', $school)
            ->getQuery()
            ->execute();
    }
}
