<?php

namespace School\SchoolDiaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TeacherSubjects
 *
 * @ORM\Table(name="teacher_subjects")
 * @ORM\Entity(repositoryClass="School\SchoolDiaryBundle\Entity\TeacherSubjectsRepository")
 */
class TeacherSubjects
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="school", type="string", length=255)
     */
    private $school;

    /**
     * @var string
     *
     * @ORM\Column(name="teacher", type="string", length=255)
     */
    private $teacher;

    /**
     * @var string
     *
     * @ORM\Column(name="subjects", type="string", length=255)
     */
    private $subjects;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set school
     *
     * @param string $school
     * @return TeacherSubjects
     */
    public function setSchool($school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return string 
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set teacher
     *
     * @param string $teacher
     * @return TeacherSubjects
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return string 
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set subjects
     *
     * @param string $subjects
     * @return TeacherSubjects
     */
    public function setSubjects($subjects)
    {
        $this->subjects = $subjects;

        return $this;
    }

    /**
     * Get subjects
     *
     * @return string 
     */
    public function getSubjects()
    {
        return $this->subjects;
    }
}
