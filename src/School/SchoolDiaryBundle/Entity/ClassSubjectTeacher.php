<?php

namespace School\SchoolDiaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClassSubjectTeacher
 *
 * @ORM\Table(name="class_subject_teacher")
 * @ORM\Entity(repositoryClass="School\SchoolDiaryBundle\Entity\ClassSubjectTeacherRepository")
 */
class ClassSubjectTeacher
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Odeljenje")
     * @ORM\JoinColumn(name="classId", referencedColumnName="id")
     */
    private $classId;

    /**
     * @ORM\ManyToOne(targetEntity="Subject")
     * @ORM\JoinColumn(name="subjectId", referencedColumnName="id")
     */
    private $subjectId;

    /**
     * @ORM\ManyToOne(targetEntity="Teacher")
     * @ORM\JoinColumn(name="teacherId", referencedColumnName="id")
     */
    private $teacherId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set classId
     *
     * @param integer $classId
     * @return ClassSubjectTeacher
     */
    public function setClassId($classId)
    {
        $this->classId = $classId;

        return $this;
    }

    /**
     * Get classId
     *
     * @return integer 
     */
    public function getClassId()
    {
        return $this->classId;
    }

    /**
     * Set subjectId
     *
     * @param integer $subjectId
     * @return ClassSubjectTeacher
     */
    public function setSubjectId($subjectId)
    {
        $this->subjectId = $subjectId;

        return $this;
    }

    /**
     * Get subjectId
     *
     * @return integer 
     */
    public function getSubjectId()
    {
        return $this->subjectId;
    }

    /**
     * Set teacherId
     *
     * @param integer $teacherId
     * @return ClassSubjectTeacher
     */
    public function setTeacherId($teacherId)
    {
        $this->teacherId = $teacherId;

        return $this;
    }

    /**
     * Get teacherId
     *
     * @return integer 
     */
    public function getTeacherId()
    {
        return $this->teacherId;
    }
}
