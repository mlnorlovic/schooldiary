<?php

namespace School\SchoolDiaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Student
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="School\SchoolDiaryBundle\Entity\StudentRepository")
 */
class Student
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="identification_number", type="string", length=255)
     */
    private $identificationNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_name", type="string", length=255)
     */
    private $parentName;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_surname", type="string", length=255)
     */
    private $parentSurname;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_email", type="string", length=255)
     */
    private $parentEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_phone", type="string", length=255)
     */
    private $parentPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_fix_phone", type="string", length=255)
     */
    private $parentFixPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=255)
     */
    private $gender;

    /**
     * @ORM\ManyToOne(targetEntity="Odeljenje", cascade={"persist"})
     * @Assert\NotBlank(message="NotBlank")
     */
    protected $odeljenje;

    /**
     * @ORM\ManyToOne(targetEntity="Odeljenje", cascade={"persist"})
     * @Assert\NotBlank(message="NotBlank")
     */
    protected $razred;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Student
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Student
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set identificationNumber
     *
     * @param string $identificationNumber
     * @return Student
     */
    public function setIdentificationNumber($identificationNumber)
    {
        $this->identificationNumber = $identificationNumber;

        return $this;
    }

    /**
     * Get identificationNumber
     *
     * @return string 
     */
    public function getIdentificationNumber()
    {
        return $this->identificationNumber;
    }

    /**
     * Set parentName
     *
     * @param string $parentName
     * @return Student
     */
    public function setParentName($parentName)
    {
        $this->parentName = $parentName;

        return $this;
    }

    /**
     * Get parentName
     *
     * @return string 
     */
    public function getParentName()
    {
        return $this->parentName;
    }

    /**
     * Set parentSurname
     *
     * @param string $parentSurname
     * @return Student
     */
    public function setParentSurname($parentSurname)
    {
        $this->parentSurname = $parentSurname;

        return $this;
    }

    /**
     * Get parentSurname
     *
     * @return string 
     */
    public function getParentSurname()
    {
        return $this->parentSurname;
    }

    /**
     * Set parentEmail
     *
     * @param string $parentEmail
     * @return Student
     */
    public function setParentEmail($parentEmail)
    {
        $this->parentEmail = $parentEmail;

        return $this;
    }

    /**
     * Get parentEmail
     *
     * @return string 
     */
    public function getParentEmail()
    {
        return $this->parentEmail;
    }

    /**
     * Set parentPhone
     *
     * @param string $parentPhone
     * @return Student
     */
    public function setParentPhone($parentPhone)
    {
        $this->parentPhone = $parentPhone;

        return $this;
    }

    /**
     * Get parentPhone
     *
     * @return string 
     */
    public function getParentPhone()
    {
        return $this->parentPhone;
    }

    /**
     * Set parentFixPhone
     *
     * @param string $parentFixPhone
     * @return Student
     */
    public function setParentFixPhone($parentFixPhone)
    {
        $this->parentFixPhone = $parentFixPhone;

        return $this;
    }

    /**
     * Get parentFixPhone
     *
     * @return string 
     */
    public function getParentFixPhone()
    {
        return $this->parentFixPhone;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Student
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Student
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @return mixed
     */
    public function getOdeljenje()
    {
        return $this->odeljenje;
    }

    /**
     * @param mixed $odeljenje
     */
    public function setOdeljenje($odeljenje)
    {
        $this->odeljenje = $odeljenje;
    }

    /**
     * @return mixed
     */
    public function getRazred()
    {
        return $this->razred;
    }

    /**
     * @param mixed $razred
     */
    public function setRazred($razred)
    {
        $this->razred = $razred;
    }
}
