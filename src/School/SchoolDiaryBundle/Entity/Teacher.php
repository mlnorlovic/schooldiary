<?php

namespace School\SchoolDiaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Teacher
 *
 * @ORM\Table(name="teacher")
 * @ORM\Entity(repositoryClass="School\SchoolDiaryBundle\Entity\TeacherRepository")
 */
class Teacher
{
    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="NotBlank")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     * @Assert\NotBlank(message="NotBlank")
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="identification_number", type="string", length=255)
     * @Assert\NotBlank(message="NotBlank")
     */
    private $identificationNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_name", type="string", length=255)
     * @Assert\NotBlank(message="NotBlank")
     */
    private $parentName;

    protected $fullName;

    /**
     * @ORM\ManyToMany(targetEntity="School", cascade={"persist"}, inversedBy="teacher")
     * @Assert\NotBlank(message="NotBlank")
     */
    protected $school;

//    /**
//     * @ORM\ManyToMany(targetEntity="Subject", cascade={"persist"}, inversedBy="teacher")
//     * @Assert\NotBlank(message="NotBlank")
//     */
//    protected $subject;

    /**
     * @ORM\OneToOne(targetEntity="School\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    public function __construct(){
        $this->school = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Teacher
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Teacher
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set identificationNumber
     *
     * @param string $identificationNumber
     * @return Teacher
     */
    public function setIdentificationNumber($identificationNumber)
    {
        $this->identificationNumber = $identificationNumber;

        return $this;
    }

    /**
     * Get identificationNumber
     *
     * @return string 
     */
    public function getIdentificationNumber()
    {
        return $this->identificationNumber;
    }

    /**
     * Set parentName
     *
     * @param string $parentName
     * @return Teacher
     */
    public function setParentName($parentName)
    {
        $this->parentName = $parentName;

        return $this;
    }

    /**
     * Get parentName
     *
     * @return string 
     */
    public function getParentName()
    {
        return $this->parentName;
    }

    /**
     * @return mixed
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * @param mixed $school
     */
    public function setSchool($school)
    {
        $this->school = $school;
    }

//    /**
//     * @return mixed
//     */
//    public function getSubject()
//    {
//        return $this->subject;
//    }
//
//    /**
//     * @param mixed $subject
//     */
//    public function setSubject($subject)
//    {
//        $this->subject = $subject;
//    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->getName() . ' ' . substr($this->getParentName(),0,1) . ' ' . $this->getSurname();
    }
}
