<?php

namespace School\SchoolDiaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subject
 *
 * @ORM\Table(name="subject")
 * @ORM\Entity(repositoryClass="School\SchoolDiaryBundle\Entity\SubjectRepository")
 */
class Subject
{
    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

//    /**
//     * @ORM\ManyToMany(targetEntity="Teacher", cascade={"persist"}, mappedBy="subject")
//     */
//    protected $teacher;

//    public function __toString()
//    {
//        return $this->name;
//    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Subject
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

//    /**
//     * @return mixed
//     */
//    public function getTeacher()
//    {
//        return $this->teacher;
//    }
//
//    /**
//     * @param mixed $teacher
//     */
//    public function setTeacher($teacher)
//    {
//        $this->teacher = $teacher;
//    }
}
